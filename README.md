# Hereditas Docker build

Contains dockerfiles for build automation.

## Getting Started



## Bugs and Issues

Have a bug or an issue with this theme? [Open a new issue](https://github.com/ultimaratioregis/hereditasdocker/issues) here on GitHub.

## Creator
Hereditas was created by and is maintained by **Mauricio Pinheiro**, Managing Partner at **Ultima Ratio Regis Informatica LTDA**.

* https://github.com/maurucao

## Copyright and License
Copyright (C) 2015-2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
Ver arquivo LICENSE para os detalhes.
